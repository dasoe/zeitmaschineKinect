#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	ofBackground(0);
    settings.addInt("camWidth", 320);
    settings.addInt("camHeight", 240);
	settings.addInt("farThreshold", 70); 
	settings.addInt("nearThreshold", 230); 
	settings.addInt("contourMin", 20); 
	settings.addInt("contourMax", 40); 
	settings.addInt("contourNumber", 4); 
	settings.addBoolean("useApproximation", false);	
	settings.addBoolean("fullscreen", true);		

	settings.addInt("lamp1RightValue"); 
	settings.addInt("lamp1LeftValue"); 
	settings.addInt("lamp2RightValue"); 
	settings.addInt("lamp2LeftValue"); 
	settings.addInt("lamp3RightValue"); 
	settings.addInt("lamp3LeftValue"); 
	settings.addInt("lamp4RightValue"); 
	settings.addInt("lamp4LeftValue"); 

	settings.init("settings.xml",true);

	lampNumber = 4;
    ofSetFrameRate(30);
    
	// enable depth->video image calibration
	kinect.setRegistration(true);

    
	//kinect.init();
	//kinect.init(true); // shows infrared instead of RGB video image
	kinect.init(false, false); // disable video image (faster fps)
	
	kinect.open();		// opens first available kinect
	//kinect.open(1);	// open a kinect by id, starting with 0 (sorted by serial # lexicographically))
	//kinect.open("A00362A08602047A");	// open a kinect using it's unique serial #
	
	// print the intrinsic IR sensor values
	if(kinect.isConnected()) {
		ofLogNotice() << "sensor-emitter dist: " << kinect.getSensorEmitterDistance() << "cm";
		ofLogNotice() << "sensor-camera dist:  " << kinect.getSensorCameraDistance() << "cm";
		ofLogNotice() << "zero plane pixel size: " << kinect.getZeroPlanePixelSize() << "mm";
		ofLogNotice() << "zero plane dist: " << kinect.getZeroPlaneDistance() << "mm";
	}
	
	width = kinect.width;
	height = kinect.height;
	
	grayThreshNear.allocate(width, height);
	grayThreshFar.allocate(width, height);
	// zero the tilt on startup
	angle = 0;
	kinect.setCameraTiltAngle(angle);
	
	kinect.setLed(ofxKinect::LED_BLINK_YELLOW_RED);
	ofSleepMillis(2000);
	kinect.setLed(ofxKinect::LED_OFF);
		
	nearThreshold = settings.getIntValue("nearThreshold");
	farThreshold = settings.getIntValue("farThreshold");
	
	grayCurrentFrame.allocate(width, height);
	
	
	ofSetFullscreen( settings.getBooleanValue("fullscreen") ); 
	
	contourMin = settings.getIntValue("contourMin"); 
	contourMax = settings.getIntValue("contourMax"); 
	contourNumber = settings.getIntValue("contourNumber"); 
	useApproximation = settings.getBooleanValue("useApproximation");	

	
	setupSerial();

	lampThresholdValues[0].x = 	settings.getIntValue("lamp1RightValue"); 
	lampThresholdValues[0].y = 	settings.getIntValue("lamp1LeftValue"); 
	lampThresholdValues[1].x = 	settings.getIntValue("lamp2RightValue"); 
	lampThresholdValues[1].y = 	settings.getIntValue("lamp2LeftValue"); 
	lampThresholdValues[2].x = 	settings.getIntValue("lamp3RightValue"); 
	lampThresholdValues[2].y = 	settings.getIntValue("lamp3LeftValue"); 
	lampThresholdValues[3].x = 	settings.getIntValue("lamp4RightValue"); 
	lampThresholdValues[3].y = 	settings.getIntValue("lamp4LeftValue"); 
	
}

//--------------------------------------------------------------
void ofApp::prepareCurrentFrame() {	// there is a new frame and we are connected
	
	if(kinect.isFrameNew()) {
		// load grayscale depth image from the kinect source
		grayCurrentFrame.setFromPixels(kinect.getDepthPixels());
		// we do two thresholds - one for the far plane and one for the near plane
		// we then do a cvAnd to get the pixels which are a union of the two thresholds
		grayThreshNear = grayCurrentFrame;
		grayThreshFar = grayCurrentFrame;
		grayThreshNear.threshold(nearThreshold, true);
		grayThreshFar.threshold(farThreshold);
		cvAnd(grayThreshNear.getCvImage(), grayThreshFar.getCvImage(), grayCurrentFrame.getCvImage(), NULL);
		// update the cv images
		grayCurrentFrame.flagImageChanged();
		// find contours which are between the size of 20 pixels and 1/3 the w*h pixels.
		// also, find holes is set to true so we will get interior contours as well....
		contourFinder.findContours(grayCurrentFrame, contourMin, contourMax, contourNumber, useApproximation);
	}
	
}

//--------------------------------------------------------------
void ofApp::update(){
		kinect.update();
	ofBackground(0);
		prepareCurrentFrame();

	settings.update();
}

//--------------------------------------------------------------
void ofApp::draw(){
			ofSetHexColor(0xffffff);
			stringstream reportStream;

			// draw from the live kinect
			kinect.drawDepth(10, 10);
			//kinect.draw(420, 10, 400, 300);
			if(kinect.hasAccelControl()) {
				reportStream << "accel is: " << ofToString(kinect.getMksAccel().x, 2) << " / "
				<< ofToString(kinect.getMksAccel().y, 2) << " / "
				<< ofToString(kinect.getMksAccel().z, 2) << endl;
			} else {
				reportStream << "Note: this is a newer Xbox Kinect or Kinect For Windows device," << endl
				<< "motor / led / accel controls are not currently supported" << endl << endl;
			}
				
			reportStream << "set near threshold " << nearThreshold << " (press: o/k)" << endl
			<< "set far threshold " << farThreshold << " (press: p/l)" << endl
			<< ", fps: " << ofGetFrameRate() << endl
			<< "connection is: " << kinect.isConnected() << endl;

			if(kinect.hasCamTiltControl()) {
				reportStream << "press UP and DOWN to change the tilt angle: " << angle << " degrees" << endl
				<< "press 1-5 & 0 to change the led mode" << endl;
			}
			grayCurrentFrame.draw(10, 320);
			contourFinder.draw(10, 320);

			ofDrawBitmapString(reportStream.str(), 20, 652);

		// clear pointsInRoom
		for (int i = 0; i < lampNumber; i++) {
			pointsInRoom[i].set(0,0,0);
		}
		
		if (indexLamp) {
			if (indexLamp > 4) {
			ofSetHexColor(0x110099);			
			ofDrawRectangle(4,4,20,20);
			ofSetHexColor(0xffffff);				
			ofDrawBitmapString( ofToString(indexLamp-4), 10,20);
			} else {
			ofSetHexColor(0x991100);			
			ofDrawRectangle(4,4,20,20);
			ofSetHexColor(0xffffff);				
			ofDrawBitmapString( ofToString(indexLamp), 10,20);
			}
		}
		
		for (int i= 0; i < contourFinder.nBlobs; i++) {
			if (i < lampNumber) {
				ofRectangle r = contourFinder.blobs.at(i).boundingRect;
				ofSetHexColor(0xffdd44);
				float tx = r.x + r.width/2;
				float ty = r.y + r.height/2;
				float tz = kinect.getDistanceAt(tx,ty);
				pointsInRoom[i].set(tx,ty,tz);
				ofDrawRectangle(tx+10,ty+320, 10,10);
				ofDrawBitmapString( ofToString(tx), i*100 + 40, 10);
				ofDrawBitmapString( ofToString(ty), i*100 + 40, 30);
				ofDrawBitmapString( ofToString(tz), i*100 + 40, 50);
				ofSetHexColor(0x00000);				
				
				positionValues[i].x = ofMap(tx, 0, width, lampThresholdValues[i].x,lampThresholdValues[i].y);
				positionValues[i].y = 20+i;
				
				ofDrawBitmapString( ofToString(positionValues[i].x), i*100 + 39, 69);

				ofSetHexColor(0xffffff);				
				ofDrawBitmapString( ofToString(positionValues[i].x), i*100 + 40, 70);

			}				
		}
		for (int i=contourFinder.nBlobs; i < 4; i++) {
				positionValues[i].x = 0;
				positionValues[i].y = 0;	
		}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	switch (key) {
		case '0':
			indexLamp = 0;
			break;
		case '1':
			indexLamp = 1;
			break;
		case '2':
			indexLamp = 2;
			break;
		case '3':
			indexLamp = 3;
			break;
		case '4':
			indexLamp = 4;
			break;
		case '5':
			indexLamp = 5;
			break;
		case '6':
			indexLamp = 6;
			break;
		case '7':
			indexLamp = 7;
			break;
		case '8':
			indexLamp = 8;
			break;



		case 'p':
			farThreshold ++;
			if (farThreshold > 255) farThreshold = 255;
			settings.setIntValue("farThreshold", farThreshold, true); 			
			break;
			
		case 'l':
			farThreshold --;
			if (farThreshold < 0) farThreshold = 0;
			settings.setIntValue("farThreshold", farThreshold, true); 
			break;
			
		case 'o':
			nearThreshold ++;
			if (nearThreshold > 255) nearThreshold = 255;
			settings.setIntValue("nearThreshold", nearThreshold, true); 
			break;
			
		case 'k':
			nearThreshold --;
			if (nearThreshold < 0) nearThreshold = 0;
			settings.setIntValue("nearThreshold", nearThreshold, true); 
			break;
		case 'i':
			kinect.setCameraTiltAngle(angle); // go back to prev tilt
			kinect.open();
			break;
			
		case 'c':
			kinect.setCameraTiltAngle(0); // zero the tilt
			kinect.close();
			break;
			
		case OF_KEY_UP:
			angle++;
			if(angle>30) angle=30;
			kinect.setCameraTiltAngle(angle);
			break;
			
		case OF_KEY_DOWN:
			angle--;
			if(angle<-30) angle=-30;
			kinect.setCameraTiltAngle(angle);
			break;
		case 's':
		sendValuesToArduino();
			break;
			
			
	}


}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}


//--------------------------------------------------------------

void ofApp::sendValuesToArduino() {
    // ------------- serial ----------------------------

	// check state of buttons/switches (here hardcoded as an example)
	bool button1 = true;
	bool button2 = true;
	bool button3 = false;
	bool button4 = true;

	if (indexLamp) {
		setupFlag = true;
	} else {
		setupFlag = false;
	}

	bitset<4> bitNumber(ofToString(setupFlag) + ofToString(button2) + ofToString(button3) + ofToString(button4));

	// generate hexed string representation (24->18; 120->78; 1101->d), leading 0 if necessary
	std::stringstream hexNumber;
	hexNumber << std::setfill('0') << std::setw(3) << std::hex << (int) positionValues[0].x 
			  << std::setfill('0') << std::setw(2) << std::hex << (int) positionValues[0].y 
			  << setfill('0') << setw(3) << hex << (int) positionValues[1].x 
			  << setfill('0') << setw(2) << hex << (int) positionValues[1].y 
			  << setfill('0') << setw(3) << hex << (int) positionValues[2].x 
			  << setfill('0') << setw(2) << hex << (int) positionValues[2].y
			  << setfill('0') << setw(3) << hex << (int) positionValues[3].x
			  << setfill('0') << setw(2) << hex << (int) positionValues[3].y
			  << hex << bitNumber.to_ulong();

    ofx::IO::ByteBuffer buffer(hexNumber.str());

   // if (aValueChanged) {
	//ofLogNotice(hexNumber.str());
        //aValueChanged = false;
	device.send(buffer);
    //}
    //ofLogNotice(hexNumber.str());


	cout << hexNumber.str();
//    // Send the byte buffer.
    // ofx::IO::PacketSerialDevice will encode the buffer, send it to the
    // receiver, and send a packet marker.

}


//--------------------------------------------------------------

void ofApp::setupSerial() {



    // ------------- serial ----------------------------

    std::vector<ofx::IO::SerialDeviceInfo> devicesInfo = ofx::IO::SerialDeviceUtils::listDevices();

    ofLogNotice("ofApp::setup") << "Connected Devices: ";

    for (std::size_t i = 0; i < devicesInfo.size(); ++i) {
        ofLogNotice("ofApp::setup") << "\t" << devicesInfo[i];
    }

    if (!devicesInfo.empty()) {
        // Connect to the first matching device.
        bool success = device.setup(devicesInfo[0], 115200);


        if (success) {
            device.registerAllEvents(this);
            ofLogNotice("ofApp::setup") << "Successfully setup " << devicesInfo[0];
        } else {
            ofLogNotice("ofApp::setup") << "Unable to setup " << devicesInfo[0];
        }
    } else {

        ofLogNotice("ofApp::setup") << "No devices connected.";
    }


}
//--------------------------------------------------------------

void ofApp::onSerialBuffer(const ofx::IO::SerialBufferEventArgs & args) {
    // Decoded serial packets will show up here.

    SerialMessage message(args.getBuffer().toString(), "", 255);
    serialMessage = message;
    ofLogNotice("#####MESSAGE: " + serialMessage.message);

}
//--------------------------------------------------------------

void ofApp::onSerialError(const ofx::IO::SerialBufferErrorEventArgs & args) {
    // Errors and their corresponding buffer (if any) will show up here.

    SerialMessage message(args.getBuffer().toString(),
            args.getException().displayText(),
            500);

    serialMessage = message;
}


//--------------------------------------------------------------
void ofApp::exit() {
	kinect.setCameraTiltAngle(0); // zero the tilt on exit
	kinect.close();	
    device.unregisterAllEvents(this);
}
