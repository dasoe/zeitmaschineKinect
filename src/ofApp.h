#pragma once

#include "ofMain.h"
#include "ofxOpenCv.h"
#include "ofxKinect.h"
#include "ofxXmlBasedProjectSettings.h"
#include "ofxSerial.h"

class SerialMessage {
public:

    SerialMessage() : fade(0) {
    }

    SerialMessage(const std::string& _message,
            const std::string& _exception,
            int _fade) :
    message(_message),
    exception(_exception),
    fade(_fade) {
    }

    std::string message;
    std::string exception;
    int fade;
};

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();
	void prepareCurrentFrame();

	void exit();
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofxKinect kinect;
		//ofxCvGrayscaleImage grayImage; // grayscale depth image
		ofxCvGrayscaleImage grayThreshNear; // the near thresholded image
		ofxCvGrayscaleImage grayThreshFar; // the far thresholded image	
		ofxCvGrayscaleImage grayCurrentFrame;

		ofxCvContourFinder contourFinder;
		int contourMin, contourMax, contourNumber;
		bool useApproximation;
			
		int nearThreshold, farThreshold, angle;
		int width, height, lampNumber;

		ofxXmlBasedProjectSettings settings;

		ofVec3f pointsInRoom[4];
		ofVec2f positionValues[4];

		ofVec2f lampThresholdValues[4];

		// serial -------------------------------------
		void setupSerial();

		bool aValueChanged;
		void sendValuesToArduino();

		void onSerialBuffer(const ofx::IO::SerialBufferEventArgs& args);
		void onSerialError(const ofx::IO::SerialBufferErrorEventArgs& args);

		ofx::IO::PacketSerialDevice device;

		SerialMessage serialMessage;
		
		int indexLamp;
		bool setupFlag;

};
